$(function() {
	//调用动态获取用户名的函数，这个函数写在了common.js里
	var name = window.localStorage.getItem("username");
	$('.DynamicUsername').text(name);
	/* 点击新建数据点按钮时，弹出填写信息框 */
	$('.createButton').click(function() {
		$('.mask,.addDataNode').show();
		
	})
	/* 点击弹出框的取消按钮时隐藏弹出层 */
	$('.cancel').click(function() {
		$('.mask,.addDataNode').hide();
		
	})
	
	/* 点击弹出框的添加按钮时将获取的数据保存到数据库之后隐藏弹出层 */
	$('.add').click(function() {
		/* 获取标识名 */
		var identificationName = $('#identificationName').val();
		/* 获取读写类型 */
		var readWriteType = $('#readWriteType').find('option:selected').text();
		/* 获取数据类型 */
		var newDataType = $('#newDataType option:selected').text();
		/* 获取备注值 */
		var remarks = $('#remarks').val();
		alert(identificationName);
		alert(readWriteType);
		alert(newDataType);
		alert(remarks);
		$('.mask,.addDataNode').hide();
	
	})
})