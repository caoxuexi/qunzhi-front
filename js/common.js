$(function() {
	$('.list').click(function() {
		if (!$('.dynamicShow').is(':visible')) {
			$('.dynamicShow').show();
		} else {
			$('.dynamicShow').hide();
		}
	})
	$('.usernameShow').mouseenter(function() {
		$('.dynamicShow').show();
	})
	$('.list').mouseleave(function() {
		$('.dynamicShow').hide();
	})

	// $('.outLogin').click(function() {
	// 	getHttp({
	// 	    url: "/user/logout",
	// 	    type: 'get',
	// 	    dataType: 'json',
	// 	    contentType: 'application/json',
	// 	}).then((res) => {
	// 		console.log(res+"+++++++++++++++")
	// 		alert("sss")
	// 		if(res.readyState == 4 && res.status == 200){
	// 				window.location.href = "../index.html"
	// 				alert("sss")
	// 		}

	// 	}).catch(res => {
	// 	    console.log(res);
	// 	})
	// })

})
/* 创建在url上传递参数，动态获取用户名的函数 */
function getUrl_To_Name() {
	// 获取 被访问时的 url
	var ur = location.href;
	// 获取该url  = 后面的name 
	var type = ur.split('?')[1].split("=")[1];
	//为其设置文本
	$('.DynamicUsername').text(type);
	//将获取的文本保存到client变量中
	var client = $('.DynamicUsername').text(type);
	//将文本内容放到clientName变量中
	var clientName = client.text();
	//获取返回产品列表的链接,并为其修改href属性值（带上参数），html页面的链接不要删，为了平稳退化
	$('#productListLink').attr('href', '../main/productList.html?username=' + clientName);
	/* 修改本页面其他链接的href属性，为其都传递username参数 */
	$('#productDetailsLink').attr('href', '../main/productDetails.html?username=' + clientName);
	$('#createProductLink').attr('href', '../main/createProduct.html?username=' + clientName);
	$('#deviceLogLink').attr('href', '../productMsg/deviceLog.html?username=' + clientName);
	$('#dataNodeLink').attr('href', '../productMsg/dataNode.html?username=' + clientName);
	$('#virtualDeviceLink').attr('href', '../productMsg/virtualDevice.html?username=' + clientName);
	$('#developDirectionLink').attr('href', '../productMsg/developDirection.html?username=' + clientName);
	$('#ApplicationConfigLink').attr('href', '../productMsg/ApplicationConfig.html?username=' + clientName);
	$('#applicationDevelopLink').attr('href', '../productMsg/applicationDevelop.html?username=' + clientName);
	$('#serviceRecommendLink').attr('href', '../productMsg/serviceRecommend.html?username=' + clientName);
	$('#firmwareUpdateLink').attr('href', '../productMsg/firmwareUpdate.html?username=' + clientName);
	$('#testingToolsLink').attr('href', '../productMsg/testingTools.html?username=' + clientName);
	$('#overviewLink').attr('href', '../productMsg/overview.html?username=' + clientName);
	$('#newOnlineLink').attr('href', '../productMsg/newOnline.html?username=' + clientName);
	$('#activeEquipmentLink').attr('href', '../productMsg/activeEquipment.html?username=' + clientName);
	$('#activeCycleLink').attr('href', '../productMsg/activeCycle.html?username=' + clientName);
	$('#connectionDurationLink').attr('href', '../productMsg/connectionDuration.html?username=' + clientName);
	$('#accountSettingLink').attr('href', '../main/accountSetting.html?username=' + clientName);
	$('#accountSafeLink').attr('href', '../main/accountSafe.html?username=' + clientName);
}


/* 退出登录 */
$('.outLogin').click(function() {
	$.ajax({
		url: 'http://222.25.188.1:48888/user/logout',
		headers: {
			"token": token //此处放置请求到的用户token
		},
		dataType: 'JSON',
		type: 'GET',
		contentType: 'application/json',
		success: function(res) {
			console.log(res)
			if (res.code == 0) {
				localStorage.clear()
				sessionStorage.clear()
				window.location.href = "../index.html"
				history.pushState(null, null, document.URL);
				window.addEventListener("popstate", function(e) {
					history.pushState(null, null, document.URL);
				}, false);
			}

		},
		error: function(res) {
			
				window.location.href = "../index.html"
			
		}
	});
});


