var baseurl = 'http://222.25.188.1:48888'
let token2 = window.localStorage.getItem('saveToken2')
let time
console.log(token2)

let projectTotal
let userTotal

$(function () {
    if (token2 == null) {
        clearInterval(time)
        window.location.href = '../index.html'
    }
    //var myWebSocket = new WebSocket("ws://222.25.188.1:48888/websocket"); //建立连接
    // var myWebSocket = new WebSocket("ws://127.0.0.1:8081/websocket"); //建立连接
    // myWebSocket.onopen = function() { //发送请求
    // 	console.log("open");
    // 	//myWebSocket.send("Hello WebSocket");
    // };
    // myWebSocket.onmessage = function(e) { //获取后端响应
    // 	console.log(e.data);
    // 	 realtimePeople = e.data.split(':')[1]
    // 	// window.localStorage.setItem("saveRealtimePeople",realtimePeople);
    // 	console.log(realtimePeople);
    // };
    // myWebSocket.onclose = function(e) {
    // 	console.log("close");
    // };
    // myWebSocket.onerror = function(e) {
    // 	console.log("error");
    // };
    var chartDom = document.getElementById('main')
    var myChart = echarts.init(chartDom)
    var option
    setInterval(function () {
        getHttp({
            url: '/adminUser/getStatistic',
            headers: {
                token: token2 //此处放置请求到的用户token
            },
            type: 'get',
            contentType: 'application/json'
        }).then(res => {
            projectTotal = res.projectCount
            userTotal = res.userCount
            onlineNum = res.onlineNum
            option = {
                backgroundColor: '#0e202d',
                title: {
                    text: '人数与项目数统计',
                    textStyle: {
                        color: '#fff',
                        fontSize: 20
                    },
                    subtextStyle: {
                        color: '#999',
                        fontSize: 16
                    },
                    x: 'center',
                    top: '5%'
                },
                grid: {
                    top: 200,
                    bottom: 150
                },
                tooltip: {},
                xAxis: {
                    data: ['开发者总数', '在线开发者数', '项目总数'],
                    axisTick: {
                        show: false
                    },
                    axisLine: {
                        show: false
                    },
                    axisLabel: {
                        interval: 0,
                        textStyle: {
                            color: '#beceff',
                            fontSize: 20
                        },
                        margin: 80 //刻度标签与轴线之间的距离。
                    }
                },
                yAxis: {
                    splitLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLine: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    }
                },
                series: [
                    {
                        name: '',
                        type: 'pictorialBar',
                        symbolSize: [100, 45],
                        symbolOffset: [0, -20],
                        z: 12,
                        data: [
                            {
                                name: '开发者总数',
                                value: userTotal,
                                trueVal: '98',
                                symbolPosition: 'end',
                                itemStyle: {
                                    normal: {
                                        color: '#00fff5' //圆柱顶部颜色
                                    }
                                }
                            },
                            {
                                name: '在线开发者数',
                                value: onlineNum,
                                trueVal: '98',
                                symbolPosition: 'end',
                                itemStyle: {
                                    normal: {
                                        color: '#ffcc00' //圆柱顶部颜色
                                    }
                                }
                            },
                            {
                                name: '项目总数',
                                value: projectTotal,
                                trueVal: '98',
                                symbolPosition: 'end',
                                itemStyle: {
                                    normal: {
                                        color: '#00fff5' //圆柱顶部颜色
                                    }
                                }
                            }
                        ]
                    },
                    {
                        name: '',
                        type: 'pictorialBar',
                        symbolSize: [100, 45],
                        symbolOffset: [0, 24],
                        z: 12,
                        data: [
                            {
                                name: '开发者总数',
                                value: userTotal,
                                trueVal: '98',
                                itemStyle: {
                                    normal: {
                                        color: '#43bafe' //圆柱底部颜色
                                    }
                                }
                            },
                            {
                                name: '在线开发者数',
                                value: onlineNum,
                                trueVal: '98',
                                itemStyle: {
                                    normal: {
                                        color: '#ff7800' //圆柱底部颜色
                                    }
                                }
                            },
                            {
                                name: '项目总数',
                                value: projectTotal,
                                trueVal: '98',
                                itemStyle: {
                                    normal: {
                                        color: '#43bafe' //圆柱底部颜色
                                    }
                                }
                            }
                        ]
                    },
                    {
                        name: '',
                        type: 'pictorialBar',
                        symbolSize: [150, 75],
                        symbolOffset: [0, 44],
                        z: 11,
                        data: [
                            {
                                name: '开发者总数',
                                value: userTotal,
                                trueVal: '98',
                                itemStyle: {
                                    normal: {
                                        color: 'transparent',
                                        borderColor: '#43bafe', //底部内圆圈颜色
                                        borderWidth: 5
                                    }
                                }
                            },
                            {
                                name: '在线开发者数',
                                value: onlineNum,
                                trueVal: '98',
                                itemStyle: {
                                    normal: {
                                        color: 'transparent',
                                        borderColor: '#ff7800', //底部内圆圈颜色
                                        borderWidth: 5
                                    }
                                }
                            },
                            {
                                name: '项目总数',
                                value: projectTotal,
                                trueVal: '98',
                                itemStyle: {
                                    normal: {
                                        color: 'transparent',
                                        borderColor: '#43bafe', //底部内圆圈颜色
                                        borderWidth: 5
                                    }
                                }
                            }
                        ]
                    },
                    {
                        name: '',
                        type: 'pictorialBar',
                        symbolSize: [200, 100],
                        symbolOffset: [0, 62],
                        z: 10,
                        data: [
                            {
                                name: '开发者总数',
                                value: userTotal,
                                trueVal: '98',
                                itemStyle: {
                                    normal: {
                                        color: 'transparent',
                                        borderColor: '#43bafe', //底部外圆圈颜色
                                        borderType: 'dashed',
                                        borderWidth: 5
                                    }
                                }
                            },
                            {
                                name: '在线开发者数',
                                value: onlineNum,
                                trueVal: '98',
                                itemStyle: {
                                    normal: {
                                        color: 'transparent',
                                        borderColor: '#ff7800', //底部外圆圈颜色
                                        borderType: 'dashed',
                                        borderWidth: 5
                                    }
                                }
                            },
                            {
                                name: '项目总数',
                                value: projectTotal,
                                trueVal: '98',
                                itemStyle: {
                                    normal: {
                                        color: 'transparent',
                                        borderColor: '#43bafe', //底部外圆圈颜色
                                        borderType: 'dashed',
                                        borderWidth: 5
                                    }
                                }
                            }
                        ]
                    },
                    {
                        type: 'bar',
                        silent: true,
                        barWidth: 100,
                        barGap: '-100%',

                        data: [
                            {
                                name: '开发者总数',
                                value: userTotal,
                                trueVal: '98',
                                label: {
                                    normal: {
                                        show: true,
                                        position: 'top',
                                        distance: 40,
                                        textStyle: {
                                            color: '#00fff5', //柱子对应数值颜色
                                            fontSize: 40
                                        }
                                    }
                                },
                                itemStyle: {
                                    normal: {
                                        color: {
                                            x: 0,
                                            y: 0,
                                            x2: 0,
                                            y2: 1,
                                            type: 'linear',
                                            global: false,
                                            colorStops: [
                                                {
                                                    offset: 0,
                                                    color: 'rgba(0,255,245,0.5)'
                                                },
                                                {
                                                    offset: 1,
                                                    color: '#43bafe' //底部渐变颜色
                                                }
                                            ]
                                        }
                                    }
                                }
                            },
                            {
                                name: '在线开发者数',
                                value: onlineNum,
                                trueVal: '98',
                                label: {
                                    normal: {
                                        show: true,
                                        position: 'top',
                                        distance: 40,
                                        textStyle: {
                                            color: '#ffcc00', //柱子对应数值颜色
                                            fontSize: 40
                                        }
                                    }
                                },
                                itemStyle: {
                                    normal: {
                                        color: {
                                            x: 0,
                                            y: 0,
                                            x2: 0,
                                            y2: 1,
                                            type: 'linear',
                                            global: false,
                                            colorStops: [
                                                {
                                                    offset: 0,
                                                    color: 'rgba(255,204,0,0.5)'
                                                },
                                                {
                                                    offset: 1,
                                                    color: '#ff7800' //底部渐变颜色
                                                }
                                            ]
                                        }
                                    }
                                }
                            },
                            {
                                name: '项目总数',
                                value: projectTotal,
                                trueVal: '98',
                                label: {
                                    normal: {
                                        show: true,
                                        position: 'top',
                                        distance: 40,
                                        textStyle: {
                                            color: '#00fff5', //柱子对应数值颜色
                                            fontSize: 40
                                        }
                                    }
                                },
                                itemStyle: {
                                    normal: {
                                        color: {
                                            x: 0,
                                            y: 0,
                                            x2: 0,
                                            y2: 1,
                                            type: 'linear',
                                            global: false,
                                            colorStops: [
                                                {
                                                    offset: 0,
                                                    color: 'rgba(0,255,245,0.5)'
                                                },
                                                {
                                                    offset: 1,
                                                    color: '#43bafe' //底部渐变颜色
                                                }
                                            ]
                                        }
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
            option && myChart.setOption(option)
        })
            .catch(res => {
                console.log(res)
            })
    }, 4000)
})

//管理员退出登录

$('.adminLogout').click(function () {
    $.ajax({
        url: 'http://222.25.188.1:48888/adminUser/logout',
        headers: {
            token: token2 //此处放置请求到的用户token
        },
        dataType: 'JSON',
        type: 'GET',
        contentType: 'application/json',
        success: function (res) {
            console.log(res)
            if (res.code == 0) {
                localStorage.clear()
                sessionStorage.clear()
                window.location.href = '../index.html'
                history.pushState(null, null, document.URL)
                window.addEventListener(
                    'popstate',
                    function (e) {
                        history.pushState(null, null, document.URL)
                    },
                    false
                )
            }
        },
        error: function (res) {
            window.location.href = '../index.html'
        }
    })
})
