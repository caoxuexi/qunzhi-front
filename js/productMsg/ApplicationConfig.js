let currentUrl = window.location.href
console.log(currentUrl)
let currentId = currentUrl.split("=")[1]
console.log(currentId)

let token = window.localStorage.getItem("saveToken");
console.log(token)

var informations = [];
$(function() {
	if (token == null) {
		window.location.href = "../index.html"
	}
	//调用动态获取用户名的函数，这个函数写在了common.js里
	var name = window.localStorage.getItem("username");
	$('.DynamicUsername').text(name);
	var Information = function(funName, microservice) {
			this.funName = funName;
			this.microservice = microservice;
		}
		$.ajax({
			async: false,
			url: "../../data/productMsg/function-micro.json",
			type: "get",
			dataType: "json",
			success: function(data) {
				$.each(data, function(i, item) {
					informations.push(new Information(item["funName"], item["microservice"]));
				})
				console.log(informations);
			},
			error: function(data) {
				console.log(data);
			}
		})
		btn();
})

 function btn() {

 		var canvas = document.getElementById("myCanvas");
 		var ctx = canvas.getContext("2d");
 		var j = 0; //功能圆的象限				
 		ctx.clearRect(0, 0, canvas.width, canvas.height);
 		for(var i = 0; i < informations.length; i++) { //多个选项时
 				ctx.fillStyle = "#f4f4f4";//设置填充颜色为灰白色				
 				ctx.strokeStyle = "#f4f4f4";//设置弧线的颜色为灰白色
 				ctx.font = '12px "微软雅黑"';
 				var funvalue = informations[i].funName; //功能的名称
 
 				var circle = xiangxian(400, 335, j, 210, 3.5);	//调用函数找功能圆的圆心			
 				var x = circle[0];
 				var y = circle[1];
 
 				ctx.beginPath();              //画功能圆
 				ctx.arc(x, y, 30, 0, Math.PI * 2, false);
 				ctx.fill();
 				ctx.stroke();				
 				ctx.closePath();
 				
 				var p = 0; 
 				for(var order = 0; order < informations[i].microservice.length; order++) {					
 					var circle=xiangxian(x, y, p++, 73, 1.5);				
 					var x1 = circle[0];
 					var y1 = circle[1];
 					
					
					
					ctx.fillStyle = "#f4f4f4";
					ctx.beginPath();
					ctx.fill();
					ctx.moveTo(x1, y1);             //画连接线
					ctx.lineTo(x, y);					
					ctx.stroke();
					ctx.closePath();	
					
					
					if(order%2==0){
						ctx.fillStyle = "pink";//设置填充颜色为	
					}else{
						ctx.fillStyle = "#bfa";//设置填充颜色为灰白色	
					}
					
					       
 					ctx.beginPath();                                //画微服务圆
 					ctx.arc(x1, y1, 26, 0, Math.PI * 2, false);
 					ctx.fill();
 					ctx.stroke();					
 					ctx.closePath();
					
 					ctx.fillStyle = "#f4f4f4";
 					ctx.beginPath();
 					ctx.fill();			
 					ctx.stroke();
 					ctx.fillStyle = "black";
 					ctx.fillText(informations[i].microservice[order], x1 - 25, y1 + 5,50);  //写微服务名称
 					ctx.closePath();			
 				}
 				ctx.beginPath();
 				ctx.fill();
 				ctx.fillStyle = "black";
 				ctx.fillText(informations[i].funName, x - 25, y + 5,51);      //写功能名称
 				ctx.closePath();
 				j++;
 		
 		};
 	};

 //将一个圆划分成num*2份，找出第j个点的坐标（从0点位置开始）
 function xiangxian( m, n, j, r, num) {          //m,n分别代表圆心的x,y坐标；r代表半径
 	var b = j * (Math.PI / num);
 	var x, y;
 	if(j == 0 || j == 1) {                      //表示第一象限
 		x = r * Math.sin(b) + m;
 		y = n - r * Math.cos(b);
 	} else if(j == 2 || j == 3) {               //表示第一象限
 		x = r * Math.sin(Math.PI - b) + m;
 		y = r * Math.cos(Math.PI - b) + n;
 	} else if(j == 4 || j == 5) {                //表示第一象限
 		x = m - r * Math.cos(Math.PI * (1.5) - b);
 		y = r * Math.sin(Math.PI * (1.5) - b) + n;
 	} else if(j == 6 || j == 7) {                //表示第一象限
 		x = m - r * Math.cos(b - Math.PI * (1.5));
 		y = n - r * Math.sin(b - Math.PI * (1.5));
 	};
 	var circle=new Array(x,y);
 	return circle;
 }

$('#productDetailsLink').click(function() {
	$('#productDetailsLink').attr("href","../main/productDetails.html?id="+currentId)
})

$('#deviceLogLink').click(function() {
	$('#deviceLogLink').attr("href","../productMsg/deviceLog.html?id="+currentId)
})

$('#developDirectionLink').click(function() {
	$('#developDirectionLink').attr("href","../productMsg/developDirection.html?id="+currentId)
})

$('#ApplicationConfigLink').click(function() {
	$('#ApplicationConfigLink').attr("href","../productMsg/ApplicationConfig.html?id="+currentId)
})

$('#serviceRecommendLink').click(function() {
	$('#serviceRecommendLink').attr("href","../productMsg/serviceRecommend.html?id="+currentId)
})

$('#applicationDevelopLink').click(function() {
	$('#applicationDevelopLink').attr("href","../productMsg/applicationDevelop.html?id="+currentId)
})

$('#MCUdevelopLink').click(function() {
	$('#MCUdevelopLink').attr("href","../productMsg/MCUdevelop.html?id="+currentId)
})

$('#overviewLink').click(function() {
	$('#overviewLink').attr("href","../productMsg/overview.html?id="+currentId)
})