var baseurl = "http://222.25.188.1:48888";

let token = window.localStorage.getItem("saveToken");
console.log(token)
$(function() {
	if (token == null) {
		window.location.href = "../index.html"
	}
	var name = window.localStorage.getItem("username");
	$('.DynamicUsername').text(name);
	//getUrl_To_Name();
	/* 设置登录邮箱名默认为登录的邮箱账号，不可更改 */
	$('.loginEmail').val($('.DynamicUsername').text());
	
	getHttp({
	    url: "/user/getInformation",
	    headers: {
	    	'token': token //此处放置请求到的用户token
	    },
	    type: 'GET',
	    contentType: 'application/json',
	}).then((res) => {
		console.log(res)
		$('.contacts').val(res.nickname);
		$('.realName').val(res.realname)
			$('.contactsEmail').val(res.email);
			$('.QQ').val(res.qq);
			$('.phoneNum').val(res.mobile)
			$('.address').val(res.address);
	}).catch(res => {
	    console.log(res);
			
	})
	
	$('.change').click(function() {
		
		/* 获取整个表单元素，存到数据库中 */
		var contacts = $('.contacts').val();
		var realName = $('.realName').val()
		var QQ = $('.QQ').val();
		var mobile = $('.phoneNum').val()
		var address = $('.address').val();
		var data1 = {
		    "nickname": contacts,
		    "realname": realName,
				"mobile":mobile,
				"qq": QQ,
				"address": address
		};
		console.log(contacts,realName,QQ,mobile,address)
		postHttp({
		    url: "/user/changeInformation",
				data: JSON.stringify(data1),
		    headers: {
		    	'token': token //此处放置请求到的用户token
		    },
		    type: 'post',
		    contentType: 'application/json',
		}).then((res) => {
			console.log(res)
			
		}).catch(res => {
		    console.log(res);
				
		})
		
		
	})
	
})