let currentUrl = window.location.href
console.log(currentUrl)
let currentId = currentUrl.split("=")[1]
console.log(currentId)

let token = window.localStorage.getItem("saveToken");
console.log(token)

$(function() {
	if (token == null) {
		window.location.href = "../index.html"
	}
	//调用动态获取用户名的函数，这个函数写在了common.js里
	var name = window.localStorage.getItem("username");
	$('.DynamicUsername').text(name);
})

function showpicture(va) {
	/* 得到所有的div标签  遍历*/
	var divs = document.getElementsByClassName("picture");
	var d0 = divs[0];
	var d1 = divs[1];
	var d2 = divs[2];	
	if(va==0){
		d0.style.backgroundImage = "url(../../images/applicationdevelop/after3.jpg)";
		d1.style.backgroundImage = "url(../../images/applicationdevelop/before2.jpg)";
		d2.style.backgroundImage = "url(../../images/applicationdevelop/before1.jpg)";
	}else if(va==1){
		d0.style.backgroundImage = "url(../../images/applicationdevelop/before3.jpg)";
		d1.style.backgroundImage = "url(../../images/applicationdevelop/after2.jpg)";
		d2.style.backgroundImage = "url(../../images/applicationdevelop/before1.jpg)";
	}else if(va==2){
		d0.style.backgroundImage = "url(../../images/applicationdevelop/before3.jpg)";
		d1.style.backgroundImage = "url(../../images/applicationdevelop/before2.jpg)";
		d2.style.backgroundImage = "url(../../images/applicationdevelop/after1.jpg)";
	}	
}

function creatsecret(){
	document.getElementById("secret").value="12ef6d183b3644a79fc7d6df52d4a3e0";
}

function creat(){
	var popBox = document.getElementById("popBox");
	var popLayer = document.getElementById("popLayer");
	popBox.style.display = "block";
	popLayer.style.display = "block";
}

	/*点击关闭按钮*/
	document.getElementById("closeBox").onclick = function() {
	    var popBox = document.getElementById("popBox");
	    var popLayer = document.getElementById("popLayer");
	    popBox.style.display = "none";
	    popLayer.style.display = "none";
	}
	
	$('#productDetailsLink').click(function() {
		$('#productDetailsLink').attr("href","../main/productDetails.html?id="+currentId)
	})
	
	$('#deviceLogLink').click(function() {
		$('#deviceLogLink').attr("href","../productMsg/deviceLog.html?id="+currentId)
	})
	
	$('#developDirectionLink').click(function() {
		$('#developDirectionLink').attr("href","../productMsg/developDirection.html?id="+currentId)
	})
	
	$('#ApplicationConfigLink').click(function() {
		$('#ApplicationConfigLink').attr("href","../productMsg/ApplicationConfig.html?id="+currentId)
	})
	
	$('#serviceRecommendLink').click(function() {
		$('#serviceRecommendLink').attr("href","../productMsg/serviceRecommend.html?id="+currentId)
	})
	
	$('#applicationDevelopLink').click(function() {
		$('#applicationDevelopLink').attr("href","../productMsg/applicationDevelop.html?id="+currentId)
	})
	
	$('#MCUdevelopLink').click(function() {
		$('#MCUdevelopLink').attr("href","../productMsg/MCUdevelop.html?id="+currentId)
	})
	
	$('#overviewLink').click(function() {
		$('#overviewLink').attr("href","../productMsg/overview.html?id="+currentId)
	})