var baseurl = "http://222.25.188.1:48888";
let token2 = window.localStorage.getItem("saveToken2");
console.log(token2)
var developer = [];
var developerSum;
$(function() {
	if (token2 == null) {
		window.location.href = "../index.html"
	}
	load(1, 9);
	
});
//管理员退出登录

$('.adminLogout').click(function() {
	$.ajax({
		url: 'http://222.25.188.1:48888/adminUser/logout',
		headers: {
			"token": token2 //此处放置请求到的用户token
		},
		dataType: 'JSON',
		type: 'GET',
		contentType: 'application/json',
		success: function(res) {
			console.log(res)
			if (res.code == 0) {
				localStorage.clear()
				sessionStorage.clear()
				window.location.href = "../index.html"
				history.pushState(null, null, document.URL);
				window.addEventListener("popstate", function(e) {
					history.pushState(null, null, document.URL);
				}, false);
			}

		},
		error: function(res) {
			
				window.location.href = "../index.html"
			
		}
	});
});
function load(pageNum, pageSize) {
	getHttp({
		url: "/adminUser/searchProject?page=" + pageNum + "&size=" + pageSize,
		type: 'get',
		dataType: 'json',
		headers: {
			'token': token2 //此处放置请求到的用户token
		},
		contentType: 'application/json',
	}).then(res => {
		console.log(res)
		developerSum = res.total;
		console.log(developerSum)
		let contentHtml =''
		//注意这里的list是后台返回的集合
		for (let item of res.list) {
			if(item.category == null) {
				item.category = '暂无'
			}
			contentHtml += `<div class="item">
							<div class="item-title">${item.name}</div>
							<hr >
							<div class="item-content">
								<div class="row1">
									<span>项目分类:</span>
									<span>${item.category}</span>
								</div>
								<div class="row2">
									<span>创建时间:</span>
									<span>${item.createTime}</span>
								</div>
								<div class="row3">
									<span>更新时间:</span>
									<span>${item.updateTime}</span>
								</div>
							</div>
						</div>`
						//将其添加到table中
						$(".itemContainer").html(contentHtml);
		}
		
		/* 让显示完整信息的div先隐藏 */
		$('.Hover').hide();
		/* 鼠标移过的时候显示 */
		$('.now').mouseover(function(event) {
			$(this).next('.Hover').css({
				"position": "absolute",
				"top": event.pageY + 5,
				"left": event.pageX + 5
			}).show();
		});

		/* 鼠标移动的过程中，动态计算位置，动态的跟随鼠标的移动去显示在合适的位置 */
		$('.now').mousemove(function(event) {
			$(this).next('.Hover').css({
				"color": "fff",
				"position": "absolute",
				"background-color": "#ff5500",
				"opacity": "0.8",
				"top": event.pageY + 5,
				"left": event.pageX + 5
			});
		});
		/* 鼠标离开的时候关闭 */
		$('.now').mouseout(function(event) {
			$(this).next('.Hover').hide();
		});

		//         //显示分页条开始
		//         /* 
		//          定义页码按钮的显示规则，最多允许显示10个按钮
		//          当小于等于10个的时候，让起始位置为1，终止位置为总页数
		//          当大于10页的时候，起始位置为当前页减5，终止页为当前页+4
		//          （可以假设，当前总页数为12页，你点击第3页，经过上面的规则，你起始页就变为-2，终止页为7，这显然
		//          不对，所以我们还要考虑特殊情况）
		//          经过上面的计算，如果起始页的数字小于1，我们就让起始位置为1，终止为止10
		//          如果计算出来的终止页大于总页数，我们也要进行处理
		//          让终止页等于总页数，起始页等于终止页-9
		//          */

		//         //假设后台传来三个参数，分别为
		//         /* 
		//          totalCount为开发者的总数
		//          totalPage为总页数
		//          res.page为当前页
		//          */
		var totalCount = developerSum;
		console.log(totalCount)
		var pageSize = 9;
		var totalPage = Math.ceil(totalCount / pageSize);
		console.log(totalPage)

		var start;
		var end;
		if (totalPage <= 10) {
			start = 1;
			end = totalPage;
		} else {
			start = res.page - 5;
			end = res.page + 4;

			if (start < 1) {
				start = 1;
				end = 10;
			}

			if (end > res.total) {
				end = res.total;
				start = end - 9;
			}
		}
		/* 接下来动态创建页码 */
		var pageHTML = ""
		pageHTML += '<li onclick="load(' + 1 + ',' + pageSize + ')"><a href="javascript:void(0)">首页</a></li>';
		if (res.page > 1) {
			pageHTML += '<li onclick="load(' + (res.page - 1) + ',' + pageSize +
				')"><a href="javascript:void(0)">上一页</a></li>';
		}

		for (var i = 1; i <= totalPage; i++) {
			if (i == pageNum) { //设置高亮
				pageHTML += ' <li class="active" onclick="load(' + i + ',' + pageSize +
					')"><a href="javascript:void(0)">' + i + '</a></li>';
			} else {
				pageHTML += ' <li onclick="load(' + i + ',' + pageSize + ')"><a href="javascript:void(0)">' +
					i + '</a></li>';
			}
		}

		if (totalPage > 1) {
			pageHTML += '<li onclick="load(' + (res.page + 1) + ',' + pageSize +
				')"><a href="javascript:void(0)">下一页</a></li>';
		}
		pageHTML += '<li onclick="load(' + totalPage + ',' + pageSize +
			')"><a href="javascript:void(0)">末页</a></li>';
		$("#pageNav").html(pageHTML);
	}).catch(res => {

	})
}
// function load(pageNum, pageSize) {
//     getHttp({
//         // url: "/admin/list?pageSize=" + pageSize + "&pageNum=" + pageNum,
//         url: "/admin/list",
//         data: {
//             "pageNum": pageNum,
//             "pageSize": pageSize
//         }
//     }).then((res) => {
//         developer = res.data.list;
//         developerSum = res.data.total;
//         var contentHtml = '';
//         //注意这里的list是后台返回的集合
//         for (var i = 0; i < developer.length; i++) {
//             //动态创建tr并展示
//             contentHtml += '<tr>' +
//                 '<td>' +
//                 '<div class="now">' + developer[i].username + '</div>' +
//                 '<div class="Hover" style="display: none;">' + developer[i].username + '</div>' +
//                 '</td>' +
//                 '<td>' + developer[i].realname + '</td>' +
//                 '<td>' +
//                 '<div class="now">' + developer[i].username + '</div>' +
//                 '<div class="Hover" style="display: none;">' + developer[i].username + '</div>' +
//                 '</td>' +
//                 '<td>' + developer[i].mobile + '</td>' +
//                 '<td>' + developer[i].qqNumber + '</td>' +
//                 '<td>' +
//                 '<div class="now">' + `${developer[i].province}` + `${developer[i].city}` + `${developer[i].country}` + '</div>' +
//                 '<div class="Hover" style="display: none;">' + `${developer[i].province}` + `${developer[i].city}` + `${developer[i].country}` + '</div>' +
//                 '</td>' +
//                 '<td>' + developer[i].addTime+ '</td>' +
//                 '</tr>';
//         }
//         //将其添加到table中
//         $("#list").html(contentHtml);


//         //显示分页的开发者信息结束

//         /* 让显示完整信息的div先隐藏 */
//         $('.Hover').hide();
//         /* 鼠标移过的时候显示 */
//         $('.now').mouseover(function(event) {
//             $(this).next('.Hover').css({
//                 "position": "absolute",
//                 "top": event.pageY + 5,
//                 "left": event.pageX + 5
//             }).show();
//         });

//         /* 鼠标移动的过程中，动态计算位置，动态的跟随鼠标的移动去显示在合适的位置 */
//         $('.now').mousemove(function(event) {
//             $(this).next('.Hover').css({
//                 "color": "fff",
//                 "position": "absolute",
//                 "background-color": "#ff5500",
//                 "opacity": "0.8",
//                 "top": event.pageY + 5,
//                 "left": event.pageX + 5
//             });
//         });
//         /* 鼠标离开的时候关闭 */
//         $('.now').mouseout(function(event) {
//             $(this).next('.Hover').hide();
//         });

//         //显示分页条开始
//         /* 
//          定义页码按钮的显示规则，最多允许显示10个按钮
//          当小于等于10个的时候，让起始位置为1，终止位置为总页数
//          当大于10页的时候，起始位置为当前页减5，终止页为当前页+4
//          （可以假设，当前总页数为12页，你点击第3页，经过上面的规则，你起始页就变为-2，终止页为7，这显然
//          不对，所以我们还要考虑特殊情况）
//          经过上面的计算，如果起始页的数字小于1，我们就让起始位置为1，终止为止10
//          如果计算出来的终止页大于总页数，我们也要进行处理
//          让终止页等于总页数，起始页等于终止页-9
//          */

//         //假设后台传来三个参数，分别为
//         /* 
//          totalCount为开发者的总数
//          totalPage为总页数
//          res.page为当前页
//          */
//         var totalCount = developerSum;
//         console.log(totalCount)
//         var pageSize = 15;
//         var totalPage = Math.ceil(totalCount / pageSize);
//         console.log(totalPage)

//         var start;
//         var end;
//         if (res.data.totalPage <= 10) {
//             start = 1;
//             end = totalPage;
//         } else {
//             start = res.page - 5;
//             end = res.page + 4;

//             if (start < 1) {
//                 start = 1;
//                 end = 10;
//             }

//             if (end > res.data.totalPage) {
//                 end = res.data.totalPage;
//                 start = end - 9;
//             }
//         }
//         /* 接下来动态创建页码 */
//         var pageHTML = ""
//         pageHTML += '<li onclick="load(' + 1 + ',' + pageSize + ')"><a href="javascript:void(0)">首页</a></li>';
//         if (res.page > 1) {
//             pageHTML += '<li onclick="load(' + (res.page - 1) + ',' + pageSize + ')"><a href="javascript:void(0)">上一页</a></li>';
//         }

//         for (var i = 1; i <= totalPage; i++) {
//             if (i == pageNum) { //设置高亮
//                 pageHTML += ' <li class="active" onclick="load(' + i + ',' + pageSize + ')"><a href="javascript:void(0)">' + i + '</a></li>';
//             } else {
//                 pageHTML += ' <li onclick="load(' + i + ',' + pageSize + ')"><a href="javascript:void(0)">' + i + '</a></li>';
//             }
//         }
//         if (res.page < res.data.totalPage) {
//             pageHTML += '<li onclick="load(' + (res.page + 1) + ',' + pageSize + ')"><a href="javascript:void(0)">下一页</a></li>';
//         }
//         pageHTML += '<li onclick="load(' + totalPage + ',' + pageSize + ')"><a href="javascript:void(0)">末页</a></li>';

//         $("#pageNav").html(pageHTML);

//         //显示分页结束
//     }).catch(res => {
//         console.log("catch");
//     })
// }
