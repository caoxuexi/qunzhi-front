var baseurl = "http://222.25.188.1:48888";
let currentUrl = window.location.href
console.log(currentUrl)
let currentId = currentUrl.split("=")[1]
console.log(currentId)

let token = window.localStorage.getItem("saveToken");
console.log(token)
var devideData;
$(function() {
	if (token == null) {
		window.location.href = "../index.html"
	}
	var name = window.localStorage.getItem("username");
	$('.DynamicUsername').text(name);
	getHttp({
		url: "/project/getLogs?id="+ currentId,
		headers: {
			'token': token //此处放置请求到的用户token
		},
		type: 'GET',
		contentType: 'application/json',
	}).then((res) => {
		devideData = res;
		console.log(res);
		for (var i = 0; i < devideData.length; i++) {
			if(devideData[i].state == 0 ) {
				devideData[i].state = '在线'
			} else if(devideData[i].state == 1) {
				devideData[i].state = '离线'
			}
			var $tr = $('<tr class="msgList">' +
				'<td id="MAC">' + devideData[i].mac + '</td>' +
				'<td id="ID">' + devideData[i].projectId + '</td>' +
				'<td>' + devideData[i].firstOnline + '</td>' +
				'<td>' + devideData[i].cumulativeOnline + '</td>' +
				'<td>' + devideData[i].lastOnline + '</td>' +
				'<td>' + devideData[i].state + '</td>' +
				'</tr>');
			var $tbody = $('#pageMain');
			$tr.appendTo($tbody);
		}
		/* 调用函数 */
		tabPage({
			pageMain: '#pageMain',
			pageNav: '#pageNav',
			pagePrev: '#prev',
			pageNext: '#next',
			/*每页显示的条数*/
			curNum: 10,
			/*高亮显示的class*/
			activeClass: 'active',
			ini: 0,
			iNum: 0
			/*初始化显示的页面*/
		});

		function tabPage(tabPage) {
			/*获取内容列表*/
			var pageMain = $(tabPage.pageMain);
			/*获取分页*/
			var pageNav = $(tabPage.pageNav);
			/*上一页*/
			var pagePrev = $(tabPage.pagePrev);
			/*下一页*/
			var pageNext = $(tabPage.pageNext);
			/*每页显示数*/
			var curNum = tabPage.curNum;
			/*计算总页数*/
			var len = Math.ceil(pageMain.find("tr").length / curNum);
			//console.log(len);
			/*生成页码*/
			var pageList = '';
			/*当前的索引值*/
			var iNum = tabPage.iNum;
			for (var i = 0; i < len; i++) {
				/* 动态的拼接出页码 */
				pageList += '<a href="javascript:;">' + (i + 1) + '</a>';
			}
			/* 将动态创建的几个页码a添加到预留下来的区域中 */
			pageNav.html(pageList);
			/*头一页加高亮显示*/
			pageNav.find("a:first").addClass(tabPage.activeClass);
			/*******标签页的点击事件*******/
			pageNav.find("a").each(function() {
				$(this).click(function() {
					pageNav.find("a").removeClass(tabPage.activeClass);
					$(this).addClass(tabPage.activeClass);
					iNum = $(this).index();
					$(pageMain).find("tr").hide();
					for (var i = ($(this).html() - 1) * curNum; i < ($(this).html()) *
						curNum; i++) {
						$(pageMain).find("tr").eq(i).show()
					}
				});
			})
			$(pageMain).find("tr").hide();
			//初始页面的显示
			for (var i = 0; i < curNum; i++) {
				$(pageMain).find("tr").eq(i).show();
			}
			/*下一页*/
			pageNext.click(function() {
				$(pageMain).find("tr").hide();
				if (iNum == len - 1) {
					/* 提示div动态显示1秒就消失 */

					/* $('#next').addClass(tabPage.pageAlertClass); */
					for (var i = (len - 1) * curNum; i < len * curNum; i++) {
						$(pageMain).find("tr").eq(i).show();
					}
					$('.nextAlert,.body').show().delay(1000).hide(0);
					/* return false; */
				} else {
					pageNav.find("a").removeClass(tabPage.activeClass);
					iNum++;
					pageNav.find("a").eq(iNum).addClass(tabPage.activeClass);
					//                    ini(iNum);
				}
				for (var i = iNum * curNum; i < (iNum + 1) * curNum; i++) {
					$(pageMain).find("tr").eq(i).show()
				}
			});
			/*上一页*/
			pagePrev.click(function() {
				$(pageMain).find("tr").hide();
				if (iNum == 0) {

					for (var i = 0; i < curNum; i++) {
						$(pageMain).find("tr").eq(i).show()
					}
					$('.prevAlert,.body').show().delay(1000).hide(0);
					/* return false; */
				} else {
					pageNav.find("a").removeClass(tabPage.activeClass);
					iNum--;
					pageNav.find("a").eq(iNum).addClass(tabPage.activeClass);
				}
				for (var i = iNum * curNum; i < (iNum + 1) * curNum; i++) {
					$(pageMain).find("tr").eq(i).show()
				}
			})
		}
		/* 实现搜索框的功能 */
		$('#searchValue').click(function() {
			/* 注意：这里必须清空，因为存在上次点击之后遗留下来的数据 */
			$('#pageMain').html("");
			/* 清空之后，重新把最初始的全部数据放进去 */
			for (var i = 0; i < 31; i++) {
				var $tr = $('<tr class="msgList">' +
					'<td id="MAC">' + devideData[i].MAC + '</td>' +
					'<td id="ID">' + devideData[i].ID + '</td>' +
					'<td>' + devideData[i].firstOnline + '</td>' +
					'<td>' + devideData[i].CumulativeOnline + '</td>' +
					'<td>' + devideData[i].lastOnline + '</td>' +
					'<td>' + devideData[i].state + '</td>' +
					'<td>' + devideData[i].operation + '</td>' +
					'</tr>');
				var $tbody = $('#pageMain');
				$tr.appendTo($tbody);
			}
			/* 获取文本框输入的内容 */
			var $txtValue = $('#log_search').val();
			/* 获取整个table中MAC和ID的内容 */
			/* var $trValue = $('#MAC #ID').html(); */
			var $filterMsg = $('.msgList').hide().filter(":contains('" + $txtValue + "')");
			if ($txtValue != '') {
				if ($filterMsg.length <= 10) {
					/* 先让其内容清空 */
					$('#pageMain').html("");
					/* 将过滤后的数据放进去 */
					$filterMsg.appendTo($('#pageMain'));
					/* 用分页的方式展示这些数据 */
					tabPage({
						pageMain: '#pageMain',
						pageNav: '#pageNav',
						pagePrev: '#prev',
						pageNext: '#next',
						/*每页显示的条数*/
						curNum: 10,
						/*高亮显示的class*/
						activeClass: 'active',
						/*初始化显示的页面*/
						ini: 0,
						iNum: 0
					});

				} else {
					/* 先让其内容清空 */
					$('#pageMain').html("");
					/* 将过滤后的数据放进去 */
					$filterMsg.appendTo($('#pageMain'));
					/* 用分页的方式展示这些数据 */
					tabPage({
						pageMain: '#pageMain',
						pageNav: '#pageNav',
						pagePrev: '#prev',
						pageNext: '#next',
						/*每页显示的条数*/
						curNum: 10,
						/*高亮显示的class*/
						activeClass: 'active',
						ini: 0,
						/*初始化显示的页面*/
						iNum: 0
					});

				}
			} else {
				tabPage({
					pageMain: '#pageMain',
					pageNav: '#pageNav',
					pagePrev: '#prev',
					pageNext: '#next',
					/*每页显示的条数*/
					curNum: 10,
					/*高亮显示的class*/
					activeClass: 'active',
					ini: 0,
					/*初始化显示的页面*/
					iNum: 0
				});
			}
		})

		//调用动态获取用户名的函数，这个函数写在了common.js里
		//getUrl_To_Name();
			
	}).catch(res => {
		console.log(res);
	})
})

$('#productDetailsLink').click(function() {
	$('#productDetailsLink').attr("href","../main/productDetails.html?id="+currentId)
})

$('#deviceLogLink').click(function() {
	$('#deviceLogLink').attr("href","../productMsg/deviceLog.html?id="+currentId)
})

$('#developDirectionLink').click(function() {
	$('#developDirectionLink').attr("href","../productMsg/developDirection.html?id="+currentId)
})

$('#ApplicationConfigLink').click(function() {
	$('#ApplicationConfigLink').attr("href","../productMsg/ApplicationConfig.html?id="+currentId)
})

$('#serviceRecommendLink').click(function() {
	$('#serviceRecommendLink').attr("href","../productMsg/serviceRecommend.html?id="+currentId)
})

$('#applicationDevelopLink').click(function() {
	$('#applicationDevelopLink').attr("href","../productMsg/applicationDevelop.html?id="+currentId)
})

$('#MCUdevelopLink').click(function() {
	$('#MCUdevelopLink').attr("href","../productMsg/MCUdevelop.html?id="+currentId)
})

$('#overviewLink').click(function() {
	$('#overviewLink').attr("href","../productMsg/overview.html?id="+currentId)
})