# 群智应用开发平台
***
# 项目介绍
***
群智应用开发平台主要用于开发者方便高效的开发产品，通过平台提供的一些功能和API等，可以快速上手，极大的提高了开发者的开发效率。
# 项目开发目录介绍
## html目前分为3个目录（index.html首页页面）直接置于html文件夹下
###. login-relogin-register-forgetpwd
- forget-password.html是忘记密码页面
- login-style.html是登录界面
- register-style.html是注册页面
###. main
- accountSafe.html是修改密码页面
- accountSetting.html是账户设置页面
- createProduct.html是创建产品页面
- productDetails.html是产品详情页面
- productList.html是产品列表页面
###. productMsg
- activeCycle.html是活跃周期页面（暂时未开发）
- activeEquipment.html是活跃设备页面（暂时未开发）
- ApplicationConfig.html是图谱关系页面（暂时未开发）
- applicationDevelop.html是应用开发页面（暂时未开发）
- connectionDuration.html是连接时长页面（暂时未开发）
- dataNode.html是数据点页面（暂时未开发）
- developDirection.html是开发向导页面
- deviceLog.html是设备日志页面
- firmwareUpdate.html是固件升级页面（暂时未开发）
- serviceRecommend.html是服务推荐页面
- MCUdevelop.html是MCU开发页面
- newOnline.html是新增上线页面（暂时未开发）
- overview.html是概览页面（暂时未开发）
- testingTools.html是产测工具页面（暂时未开发）
- virtualDevice.html是虚拟设备页面（暂时未开发）
***
## css目录页面结构和html基本一样，css文件名对应html的文件名  

需要说明的是：  
  
common.css是头部和尾部的公共样式  
 
swiper-bundle.min.css是首页轮播图引用的样式  

			
***
## js目录页面结构和html基本一样，js文件名对应html的文件名  

需要说明的是：  
  
login-register-forgetpwd目录下多了一个zxcvbn.js文件，该文件用于实现验证码  

common.js是多个页面都用的功能（具体是鼠标点击或者经过用户名时会弹出用户个人信息修改框）  
  
alert.js是产品详情页用到的自定义弹出框   

swiper-bundle.min.js是首页轮播图引用的js文件   

http.js封装了ajax请求  

*** 
## fonts目录是部分页面用到的字体图标
***
## images目录是整个项目用到的图片
*** 
## data目录是模拟请求的一些json数据
# 项目版本
***
2021/03/26{SunJunFei}  

1、产品详情页点击删除时，弹出自定义弹出框  

2、新增鼠标经过用户名，弹出个人信息修改和退出登录框  

3、产品详情中开发向导页面的构建以及部分逻辑的实现  

4、新增用户账号设置及修改密码页面