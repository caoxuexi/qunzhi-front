var baseurl = "http://222.25.188.1:48888";
var productMsgData;
// productMsg();

let token = window.localStorage.getItem("saveToken");
console.log(token)

/* 动态创建产品详细展示 */
$(function() {
	if (token == null) {
		window.location.href = "../index.html"
	}
	var name = window.localStorage.getItem("username");
	$('.DynamicUsername').text(name);
	var myWebSocket = new WebSocket("ws://222.25.188.1:48888/websocket"); //建立连接
	myWebSocket.onopen = function() { //发送请求
		console.log("open");
		//myWebSocket.send("Hello WebSocket");
	};
	myWebSocket.onmessage = function(e) { //获取后端响应
		console.log(e.data);
		let realtimePeople = e.data.split(':')[1]
		window.localStorage.setItem("saveRealtimePeople",realtimePeople);
		console.log(realtimePeople);
	};
	myWebSocket.onclose = function(e) {
		console.log("close");
	};
	myWebSocket.onerror = function(e) {
		console.log("error");
	};
	$.ajax({
		url: "http://222.25.188.1:48888/project/listAllByUser",
		headers: {
			'token': token //此处放置请求到的用户token
		},
		type: 'GET',
		contentType: 'application/json',
		success: function(res) {
			productMsgData = res;
			console.log(res)
			/* // 获取 被访问时的 url
			var ur = location.href;
			// 获取该url  = 后面的name 
			var type = ur.split('?')[1].split("=")[1];
			//为其设置文本
			$('.DynamicUsername').text(type);
			var client = $('.DynamicUsername').text(type);
			var clientName = client.text();
			$('#createProductLink').attr('href', '../main/createProduct.html?username=' + clientName);
			$('#accountSettingLink').attr('href','../main/accountSetting.html?username='+clientName); */
			$('#createProductLink').attr('href', '../main/createProduct.html');
			$('#accountSettingLink').attr('href', '../main/accountSetting.html');
			for (var i = 0; i < res.length; i++) {
				var parentdiv = $('<div></div>');
				parentdiv.attr('class', 'productDetailsShow');
				parentdiv.attr('id', 'msgShow');
				var alink = $('<a></a>');
				/* alink.attr('href', '../../html/main/productDetails.html?username='+clientName); */
				alink.attr('href', '../../html/main/productDetails.html?id='+ res[i].id );
				var grandsondiv = $('<div></div>');
				grandsondiv.attr('class', 'productDetailsShow_hd');
				var grandsondivSpan = $('<span></span>');
				grandsondivSpan.text(productMsgData[i].name);
				grandsondivSpan.appendTo(grandsondiv);
				grandsondiv.appendTo(alink);
			
			
				var hr = $('<hr>');
			
			
				var sondiv = $('<div></div>');
				sondiv.attr('class', 'productDetailsShow_bd');
			
				var oneh4 = $('<h4></h4>');
				var span1 = $('<span></span>');
				span1.attr('class', 'small');
				span1.text('Product Key：');
				var span2 = $('<span></span>');
				span2.attr('class', 'smalls');
				span2.text(productMsgData[i].productKey);
				span1.appendTo(oneh4);
				span2.appendTo(oneh4);
			
				var date = new Date(productMsgData[i].createTime);
			
				var twoh4 = $('<h4></h4>');
				var span3 = $('<span></span>');
				span3.attr('class', 'cName');
				span3.text('创建时间:');
				var span4 = $('<span>' + date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date
					.getDate() + '</span>');
				span4.attr('class', 'cContent');
				span3.appendTo(twoh4);
				span4.appendTo(twoh4);
			
				var date2 = new Date(productMsgData[i].updateTime);
				var threeh4 = $('<h4></h4>');
				var span5 = $('<span></span>');
				span5.attr('class', 'uName');
				span5.text('更新时间:');
				var span6 = $('<span>' + date2.getFullYear() + "-" + (date2.getMonth() + 1) + "-" + date2
					.getDate() + '</span>');
				span6.attr('class', 'uContent');
				span5.appendTo(threeh4);
				span6.appendTo(threeh4);
			
			
				var sbutton = $('<button></button>');
				sbutton.text('在线调试设备');
			
				oneh4.appendTo(sondiv);
				twoh4.appendTo(sondiv);
				threeh4.appendTo(sondiv);
				sbutton.appendTo(sondiv);
			
				alink.appendTo(parentdiv);
				hr.appendTo(parentdiv);
				sondiv.appendTo(parentdiv);
			
				
				parentdiv.appendTo($('#personGridwrapper'));
				
			}
			
			if ($('.grid-wrapper').html() == "") {
				$('.productNone').show();
			}
		},
		error: function(res) {
			console.log(res)
		}
	})
})

/* 实现搜索框的功能 */
function txtSearch() {
	//遍历移除b标签，防止第二次搜索bug
	$(".txtChangeStyle").each(function() {
		var xx = $(this).html();
		$(this).replaceWith(xx);
	});
	/* 整个产品信息div */
	var str = $('#personGridwrapper,#teamGridwrapper').html();
	/* 文本输入框 */
	var txt = $('#txtSearch').val();
	/* 如果输入框内容不为空 */
	if ($.trim(txt) !== "") {
		/* 定义b标签样式红色加粗 */
		/* var re = "<b class='txtChangeStyle'>" + txt + "</b>"; */
		/* 替换搜索相关的所有内容 */
		/* var nn = str.replace(new RegExp(txt, "gm"), re); */
		/* 赋值 */
		/* $(".grid-wrapper").html(nn); */
		/* 显示搜索内容相关的div */
		$(".productDetailsShow").hide().filter(":contains('" + txt + "')").show();
	} else {
		$(".productDetailsShow").show();
	}
}