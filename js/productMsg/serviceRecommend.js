let currentUrl = window.location.href
console.log(currentUrl)
let currentId = currentUrl.split("=")[1]
console.log(currentId)


let token = window.localStorage.getItem("saveToken");
console.log(token)

$(function() {
	if (token == null) {
		window.location.href = "../index.html"
	}
	var name = window.localStorage.getItem("username");
	$('.DynamicUsername').text(name);
	var Information = function(funName, microservice, number) {
		this.funName = funName;
		this.microservice = microservice;
		this.number = number;
	}
	var informations = [];

	$.ajax({
		async: false,
		url: "../../data/productMsg/serviceRecommend.json",
		type: "get",
		dataType: "json",
		success: function(data) {
			$.each(data, function(i, item) {
				informations.push(new Information(item["funName"], item["microservice"],
					item["number"]));
			})
		},
		error: function(data) {
			console.log(data);
		}
	})

	var colors = [];
	$.ajax({
		async: false,
		url: "../../data/productMsg/color.json",
		type: "get",
		dataType: "json",
		success: function(data) {
			$.each(data, function(i, item) {
				colors.push(item["color"]);
			})
		},
		error: function(data) {
			console.log(colors);
		}
	})
	var numberss = [];
	var tableobj = document.getElementById("tianjia");
	var j = 0;

	for (var i = 0; i < informations.length; i++) {
		

		var obj = informations[i];
		numberss.push(obj["number"]);
		if (obj["number"] == 1) {
			var trobj = document.createElement("tr");
			trobj.classList.add("tr-edit1");
			console.log(informations[i]);

			var op = obj["funName"];
			var tdobj = document.createElement("td");
			tdobj.innerHTML = '<div class="function-input">' + op + '</div>';
			trobj.appendChild(tdobj);

			var op = obj["microservice"];
			var tdobj = document.createElement("td");
			tdobj.innerHTML = '<div class="microservice-input"><div class="little-microservice">' + op +
				'</div></div>';
			trobj.appendChild(tdobj);

		} else if (obj["number"] == 2) {
			var trobj = document.createElement("tr");
			trobj.classList.add("tr-edit2");
			console.log(informations[i]);

			var op = obj["funName"];
			var tdobj = document.createElement("td");
			tdobj.innerHTML = '<div class="function-input">' + op + '</div>';
			trobj.appendChild(tdobj);

			var op = obj["microservice"];
			var tdobj = document.createElement("td");
			tdobj.innerHTML = '<div class="microservice-input"><div class="little-microservice">' + op[0] +
				'</div><div class="little-microservice">' + op[1] + '</div></div>';
			trobj.appendChild(tdobj);

		} else if (obj["number"] == 3) {
			var trobj = document.createElement("tr");
			trobj.classList.add("tr-edit3");
			console.log(informations[i]);

			var op = obj["funName"];
			var tdobj = document.createElement("td");
			tdobj.innerHTML = '<div class="function-input">' + op + '</div>';
			trobj.appendChild(tdobj);

			var op = obj["microservice"];
			var tdobj = document.createElement("td");
			tdobj.innerHTML = '<div class="microservice-input"><div class="little-microservice">' + op[0] +
				'</div><div class="little-microservice">' + op[1] + '</div><div class="little-microservice">' +
				op[2] + '</div></div>';
			trobj.appendChild(tdobj);
		}

		trobj.appendChild(tdobj);
		tableobj.appendChild(trobj);
		// if ((i == 0) || (i == 1 && informations.length > 2)) {
		// 	var imagediv = document.createElement("div");
		// 	imagediv.classList.add("little-image-div");
		// 	imagediv.innerHTML = '<img src="../../images/logic.png" class="little-image" />';
		// 	tableobj.appendChild(imagediv);
		// 	console.log("箭头");
		// }
		// if (i>1) {
		// 	var table = document.getElementById("tianjia");
		// 	var trs = table.getElementsByTagName('tr');
		// 	trs[i+1].style.background = "#C1C1C1";
		// 	console.log("背景");
		// }

		
		
	}
	var element1 = document.getElementsByClassName("function-input");
	var element2 = document.getElementsByClassName("little-microservice");
	var nn = 0;
	for (var i = 0; i < informations.length; i++) {
		element1[i].style.backgroundColor = colors[j];
		for (var a = 0; a < numberss[i]; a++) {
			element2[nn++].style.backgroundColor = colors[j];
		}
		j++;
	}



})
$('#productDetailsLink').click(function() {
	$('#productDetailsLink').attr("href","../main/productDetails.html?id="+currentId)
})

$('#deviceLogLink').click(function() {
	$('#deviceLogLink').attr("href","../productMsg/deviceLog.html?id="+currentId)
})

$('#developDirectionLink').click(function() {
	$('#developDirectionLink').attr("href","../productMsg/developDirection.html?id="+currentId)
})

$('#ApplicationConfigLink').click(function() {
	$('#ApplicationConfigLink').attr("href","../productMsg/ApplicationConfig.html?id="+currentId)
})

$('#serviceRecommendLink').click(function() {
	$('#serviceRecommendLink').attr("href","../productMsg/serviceRecommend.html?id="+currentId)
})

$('#applicationDevelopLink').click(function() {
	$('#applicationDevelopLink').attr("href","../productMsg/applicationDevelop.html?id="+currentId)
})

$('#MCUdevelopLink').click(function() {
	$('#MCUdevelopLink').attr("href","../productMsg/MCUdevelop.html?id="+currentId)
})

$('#overviewLink').click(function() {
	$('#overviewLink').attr("href","../productMsg/overview.html?id="+currentId)
})