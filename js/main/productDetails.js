var baseurl = "http://222.25.188.1:48888";

let token = window.localStorage.getItem("saveToken");
console.log(token)

let currentUrl = window.location.href
console.log(currentUrl)
let currentId = currentUrl.split("=")[1]
console.log(currentId)
var detailData;
/* detailsData(); */
$(function() {
	if (token == null) {
		window.location.href = "../index.html"
	}
	var name = window.localStorage.getItem("username");
	$('.DynamicUsername').text(name);
	getHttp({
		url: "/project/detail?id=" + currentId ,
		headers: {
			'token': token //此处放置请求到的用户token
		},
		type: 'GET',
		contentType: 'application/json',
	}).then((res) => {
		// detailData = res;
		console.log(res);
		
		var form = $('#form_test');
		let contentHtml = `<table>
												<tbody>
													<tr>
														<td class="td_left">项目名称:</td>
														<td class="td_right">${res.name}</td>
													</tr>
													<tr>
														<td class="td_left">项目类型:</td>
														<td class="td_right">${res.category == null ? '暂无' : res.category}</td>
													</tr>
													<tr>
														<td class="td_left">通讯方式:</td>
														<td class="td_right">${res.chatMethod == null ? '暂无' : res.chatMethod}</td>
													</tr>
													<tr>
														<td class="td_left">Product Key:</td>
														<td class="td_right">${res.productKey}</td>
													</tr>
													<tr>
														<td class="td_left">Product Secret:</td>
														<td class="td_right">${res.productSecret == null ? '暂无' : res.productSecret}</td>
													</tr>
													<tr>
														<td class="td_left">功耗类型:</td>
														<td class="td_right">${res.powerDissipation == 1 ? '不正常' : '正常'}</td>
													</tr>
													<tr>
														<td class="td_left">创建时间:</td>
														<td class="td_right">${res.createTime}</td>
													</tr>
													<tr>
														<td class="td_left">更新时间:</td>
														<td class="td_right">${res.updateTime}</td>
													</tr>
													
												</tbody>
													
											</table>`
		form.html(contentHtml)
		
		//调用动态获取用户名的函数，这个函数写在了common.js里
		//getUrl_To_Name();
	}).catch(res => {
		console.log("catch");
	})
})

// 删除项目

$('.delete').click(function() {
		myConfirm("删除提醒", "您确定要删除该产品吗？不可恢复", function(msg) {
			
			if (msg) {
				deleteHttp({
				    url: "/project/delete?id="+ currentId,
				    type: 'DELETE',
				    dataType: 'json',
						headers: {
							'token': token //此处放置请求到的用户token
						},
				    contentType: 'application/json',
				}).then((res) => {
					/* 要进行删除的内容信息 */
					myAlert("删除提醒", "信息已删除");
						window.open("../html/main/productList.html","_self");
				}).catch(res => {
				    console.log(res);
						
				})
				
				/* window.open("../html/main/productList.html","_self") */
			}
		})
	})



$('#productDetailsLink').click(function() {
	$('#productDetailsLink').attr("href","../main/productDetails.html?id="+currentId)
})

$('#deviceLogLink').click(function() {
	$('#deviceLogLink').attr("href","../productMsg/deviceLog.html?id="+currentId)
})

$('#developDirectionLink').click(function() {
	$('#developDirectionLink').attr("href","../productMsg/developDirection.html?id="+currentId)
})

$('#ApplicationConfigLink').click(function() {
	$('#ApplicationConfigLink').attr("href","../productMsg/ApplicationConfig.html?id="+currentId)
})

$('#serviceRecommendLink').click(function() {
	$('#serviceRecommendLink').attr("href","../productMsg/serviceRecommend.html?id="+currentId)
})

$('#applicationDevelopLink').click(function() {
	$('#applicationDevelopLink').attr("href","../productMsg/applicationDevelop.html?id="+currentId)
})

$('#MCUdevelopLink').click(function() {
	$('#MCUdevelopLink').attr("href","../productMsg/MCUdevelop.html?id="+currentId)
})


$('#overviewLink').click(function() {
	$('#overviewLink').attr("href","../productMsg/overview.html?id="+currentId)
})


