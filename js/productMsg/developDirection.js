let currentUrl = window.location.href
console.log(currentUrl)
let currentId = currentUrl.split("=")[1]
console.log(currentId)

let token = window.localStorage.getItem("saveToken");
console.log(token)

var developDirection;
$(function() {
	if (token == null) {
		window.location.href = "../index.html"
	}
	var name = window.localStorage.getItem("username");
	$('.DynamicUsername').text(name);
	getHttp({
		url: "/data/productMsg/developDirection.json"
	}).then((res) => {
		developDirection = res;
		console.log(res);
		for (var i = 0; i < 20; i++) {
			var $div = $('<div class="funDevelop_Details">' +
				'<p class="funName">' + developDirection[i].fName + '</p>' +
				'<div class="brief_describe">' + developDirection[i].fDescript + '</div>' +
				'</div>');
			$div.appendTo($('.details'));
		}
		/* 循环创建20个显示微服务div */
		for (var m = 0; m < 20; m++) {
			var $mircoServer = $('<div class="mircoServer"></div')
			$mircoServer.addClass('mircoServer' + m);
			$mircoServer.appendTo($('.funDevelop'));
		}
		/* 给上面创建的20个微服务div添加内容 */
		for (var k = 0; k < 20; k++) {
			var $microTitle = $('<p class="microTitle">' + developDirection[k].fName + '</p>');
			$microTitle.appendTo($('.mircoServer' + k));
			/* 用for...in 是因为这里的json冒号后面是一个数组，为了得到这些下标，从而添加多个微服务*/
			for (t in developDirection[k].mircoServerName) {
				var $mirco = $('<p class="microName">' + developDirection[k].mircoServerName[t] +
					'</p>' +
					'<p class="mircoDescript">' + developDirection[k].mircoServerContent[t] + '</p>'
					);
				$mirco.appendTo($('.mircoServer' + k));
			}
		}
		$('.details .funDevelop_Details').click(function() {
			var i = $(this).index();
			/* 匹配到对应的去展示，当点击另一个时，只显示当前点击的这个，其他和他类名一样的div全部关闭 */
			$('.mircoServer').eq(i).show().siblings().filter(".mircoServer").hide();
			$('.funDevelop').mouseleave(function() {
				$('.mircoServer').hide(1000);
			});
		});

		//调用动态获取用户名的函数，这个函数写在了common.js里
		//getUrl_To_Name();

	}).catch(res => {
		console.log("catch");
	})

});
$('#productDetailsLink').click(function() {
	$('#productDetailsLink').attr("href","../main/productDetails.html?id="+currentId)
})

$('#deviceLogLink').click(function() {
	$('#deviceLogLink').attr("href","../productMsg/deviceLog.html?id="+currentId)
})

$('#developDirectionLink').click(function() {
	$('#developDirectionLink').attr("href","../productMsg/developDirection.html?id="+currentId)
})

$('#ApplicationConfigLink').click(function() {
	$('#ApplicationConfigLink').attr("href","../productMsg/ApplicationConfig.html?id="+currentId)
})

$('#serviceRecommendLink').click(function() {
	$('#serviceRecommendLink').attr("href","../productMsg/serviceRecommend.html?id="+currentId)
})

$('#applicationDevelopLink').click(function() {
	$('#applicationDevelopLink').attr("href","../productMsg/applicationDevelop.html?id="+currentId)
})

$('#MCUdevelopLink').click(function() {
	$('#MCUdevelopLink').attr("href","../productMsg/MCUdevelop.html?id="+currentId)
})

$('#overviewLink').click(function() {
	$('#overviewLink').attr("href","../productMsg/overview.html?id="+currentId)
})