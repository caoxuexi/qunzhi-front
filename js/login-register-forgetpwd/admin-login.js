var baseurl = "http://222.25.188.1:48888";
let projectTotal;
$(function() {
	$('#btn').click(function() {
		var name = $('#username').val().trim();
		var pwd = $('#password').val().trim();
		if (name.length == 0 || pwd.length == 0) {
			$('.main-form-errMsg').show().html("您输入的账号或密码为空，请重新输入");
		} else {
			var data1 = {
				"email": name,
				"password": pwd
			};
			postHttp({
				url: "/adminUser/login",
				data: JSON.stringify(data1),
				type: 'post',
				dataType: 'json',
				contentType: 'application/json',
			}).then((res) => {
				console.log(res)
				let token2 = res.token
				window.localStorage.setItem("saveToken2",token2);
				window.location.href = '../../html/main/admin-index.html';
			}).catch(res => {
				console.log("catch");
				$('.main-form-errMsg').show().html("您输入的账号或密码有误，请重新输入");
			})
		}
	})
})



function postHttp(request) {
	// let request = {
	//  type: "请求方式",
	//  data: "传递的参数",
	// 	url: "请求的地址",
	// 	contentType: "数据的类型",
	// 	dataType: "返回后的数据格式"
	// }
	return new Promise(function(resolve, reject) {
		$.ajax({
			type: "POST",
			url: baseurl + request.url,
			data: request.data || {},
			contentType: request.contentType || "application/json",
			dataType: request.dataType || "json",
			success: function(res) {
				resolve(res)
				// res.success ? resolve(res) : reject(res)
			},
			error: function(res) {
				reject(res)
				// reject(res)
			}
		})
	})
}
