var baseurl = "http://222.25.188.1:48888";

let token = window.localStorage.getItem("saveToken");
console.log(token)
$(function() {
	if (token == null) {
		window.location.href = "../index.html"
	}
	var name = window.localStorage.getItem("username");
	$('.DynamicUsername').text(name);
	/* // 获取 被访问时的 url
	var ur = location.href;
	// 获取该url  = 后面的name 
	var type = ur.split('?')[1].split("=")[1];
	//为其设置文本
	$('.DynamicUsername').text(type);
	//将获取的文本保存到client变量中
	var client = $('.DynamicUsername').text(type);
	//将文本内容放到clientName变量中
	var clientName = client.text();
	$('#accountSettingLink').attr('href', 'accountSetting.html?username=' + clientName);
	$('#accountSafeLink').attr('href', 'accountSafe.html?username=' + clientName); */
	/* getUrl_To_Name(); */

	/* 旧密码输入框失去焦点时 */
	// $("#oldpassword").blur(function() {
	// 	var param = $("#oldpassword").val();
	// 	/* 用ajax验证是否和原来的旧密码相同 */
	// 	$.ajax({
	// 		url: "../../data/login-data.json",
	// 		async: false,
	// 		type: "get",
	// 		dataType: "json",
	// 		success: function(data) {
	// 			$.each(data, function(i, item) {
	// 				if (param == item["password"]) {
	// 					$("#tip1").html(
	// 						"<span>密码输入正确</span>").css({'color':'green','marginLeft':'15px'});
	// 				} else {
	// 					$("#tip1").html(
	// 						"<span >密码输入错误</span>").css({'color':'red','marginLeft':'15px'});
	// 				}
	// 			});
	// 		}
	// 	});
	// });
	/* 输入新密码 */
	$("#password1").blur(function() {
		var num = $("#password1").val().length;
		if (num < 6) {
			$("#tip2").html("<span>密码太短，请重新输入</span>").css({
				'color': 'red',
				'marginLeft': '15px'
			});
		} else if (num > 18) {
			$("#tip2").html("<span>密码太长，请重新输入</span>").css({
				'color': 'red',
				'marginLeft': '15px'
			});
		} else {
			$("#tip2").html("<span>验证通过</span>").css({
				'color': 'green',
				'marginLeft': '15px'
			});
		}
	});
	/* 再次输入新密码 */
	$("#password2").blur(function() {
		var tmp = $("#password1").val();
		var num = $("#password2").val().length;
		if ($("#password2").val() != tmp) {
			$("#tip3").html("<span>两次密码输入不一致，请重新输入</span>").css({
				'color': 'red',
				'marginLeft': '15px'
			});
		} else {
			if (num >= 6 && num <= 18) {
				$("#tip3").html("<span>验证通过</span>").css({
					'color': 'green',
					'marginLeft': '15px'
				});
			} else {
				$("#tip3").html("<span>验证不通过，无效</span>").css({
					'color': 'red',
					'marginLeft': '15px'
				});
			}
		}
	});

	/* 绑定保存按钮的点击事件 */
	$(".change").click(function() {

		var flag = true;
		var old = $("#oldpassword").val();
		var pass = $("#password1").val();
		var pass2 = $("#password2").val();
		var num1 = $("#password1").val().length;
		var num2 = $("#password2").val().length;
		var data1 = {
			"oldPassword": old,
			"newPassword": pass,
			"reNewPassword": pass2
		}
		if (num1 != num2 || num1 < 6 || num2 < 6 || num1 > 18 || num2 > 18 || pass != pass2) {
			flag = false;
		} else {
			flag = true;
		}
		if (flag) {
			postHttp({
				url: "/user/changePassword",
				data: JSON.stringify(data1),
				headers: {
					'token': token //此处放置请求到的用户token
				},
				type: 'post',
				contentType: 'application/json',
			}).then((res) => {
				console.log(res)
				$("#tip4").show().html("<span>保存成功</span>").css({
					'color': 'green',
					'fontSize': '18px',
					'textAlign': 'center',
					'marginLeft': '100px',
					'marginTop': '20px',
					'width': '500px',
					'height': "60px",
					'lineHeight': '60px',
					'backgroundColor': '#fff',
					'boxShadow': '0 0 5px #000',
					'borderRadius': '5px'
				});
				$("#oldpassword").val("");
				$("#password1").val("");
				$("#password2").val("");
				$("#tip1").empty();
				$("#tip2").empty();
				$("#tip3").empty();
				$("#tip4").delay(2000).hide(0);
			}).catch(res => {
				console.log(res);

			})
		} else {
			if ($('#oldpassword').val().length == 0 || $('#password1').val().length == 0 || $(
					'#password2').val().length == 0) {
				$("#tip4").show().html("<span>输入信息不完整，请重输入</span>").css({
					'color': 'red',
					'marginLeft': '100px',
					'marginTop': '20px'
				});
			} else {
				$("#tip4").show().html(
					"<span>输入有误，请重新输入</span>"
				).css({
					'color': 'red',
					'marginLeft': '100px',
					'marginTop': '20px'
				});
			}

		}
	});
	$('#accountSettingLink').click(function() {
		getHttp({
		    url: "/user/getInformation",
		    headers: {
		    	'token': token //此处放置请求到的用户token
		    },
		    type: 'GET',
		    contentType: 'application/json',
		}).then((res) => {
			console.log(res)
			$('.contacts').val(res.nickname);
			$('.realName').val(res.realname)
				$('.contactsEmail').val(res.email);
				$('.QQ').val(res.qq);
				$('.phoneNum').val(res.mobile)
				$('.address').val(res.address);
		}).catch(res => {
		    console.log(res);
				
		})
	})
});
